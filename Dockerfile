FROM sharelatex/sharelatex:latest

# MAINTAINER Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>

RUN apt-get update && apt-get dist-upgrade -y

RUN apt-get update && apt-get install -y \
    subversion \
    git \
    texlive texlive-full \
    gdal-bin \
    libgdal-dev \
    libgeos-dev \
    libspatialite-dev \
    proj-bin \
    proj-data \
    libproj-dev \
    libcurl4-openssl-dev \
    libnetcdf-dev \
    netcdf-bin \
    mono-runtime \
    libmono-microsoft-visualbasic10.0-cil \
    'libmono-2.0-1' \
    'libmono-corlib2.0-cil' \
    gsl-bin \
    libgsl0-dev \
    mesa-common-dev \
    libglu1-mesa-dev \
    postgis \
    postgresql-client

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9

COPY cran_rstudio_com_bin_linux_ubuntu.list /etc/apt/sources.list.d/cran_rstudio_com_bin_linux_ubuntu.list

RUN apt-get update && apt-get install -y \
    r-recommended \
    r-base \
    r-base-dev \
    r-base-html \
    r-base-core \
    r-doc-html \
    r-cran-boot \
    r-cran-cluster \
    r-cran-foreign \
    r-cran-kernsmooth \
    r-cran-lattice \
    r-cran-mgcv \
    r-cran-nlme \
    r-cran-rpart \
    r-cran-survival \
    r-cran-mass \
    r-cran-class \
    r-cran-nnet \
    r-cran-spatial \
    r-cran-codetools \
    r-cran-matrix \
    jags


# Install some R packages that are required by the custom Rprofile.site
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='jsonlite', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='googleVis', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='httr', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='XML', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='DBI', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='RPostgreSQL', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"
RUN Rscript --slave --no-save --no-restore-history -e "install.packages(pkgs='knitr', repos=c('https://cran.mirror.garr.it/mirrors/CRAN/'));"

RUN cd srv ;  wget https://github.com/jgm/pandoc/releases/download/2.2.2.1/pandoc-2.2.2.1-1-amd64.deb

RUN dpkg -i /srv/pandoc-2.2.2.1-1-amd64.deb

RUN apt-get update && apt-get install -y cron

# copy the LatexMk file needed by knitr
COPY LatexMk /etc/LatexMk

RUN mkdir -p /srv/d4science

RUN cd /srv/d4science ; svn checkout http://svn.research-infrastructures.eu/public/d4science/gcube/trunk/data-analysis/RConfiguration/RStudioConfiguration

RUN rm -f /etc/R/Rprofile.site && ln -s /srv/d4science/RStudioConfiguration/.Rprofile /etc/R/Rprofile.site

RUN mkdir -p /srv/r_updater

RUN cd /srv/r_updater ; svn checkout http://svn.research-infrastructures.eu/public/d4science/gcube/trunk/data-analysis/RConfiguration/RPackagesManagement

RUN mkdir -p /etc/cron.d
COPY install-r-packages.cron /etc/cron.d/install-r-packages
COPY upgrade-r-packages.cron /etc/cron.d/upgrade-r-packages
COPY update-rprofile.cron /etc/cron.d/update-rprofile
COPY update_r_packages /usr/local/bin/update_r_packages
RUN chmod 755 /usr/local/bin/update_r_packages

COPY settings.coffee /etc/sharelatex/settings.coffee

EXPOSE 80

WORKDIR /

ENTRYPOINT ["/sbin/my_init"]

