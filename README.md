# README file of the D4Science ShareLatex repository

## Authors: Andrea Dell'Amico (ISTI-CNR)

### Creation date: July 25th, 2018

### ALst update: July 30th, 2018

## I. Content

* Dockerfile                            file used to build a docker image
* Rprofile.site                         file used to set R local options
* README.md                             current information text file
